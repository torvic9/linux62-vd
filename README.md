## vd kernel 6.2 for Arch-compatible desktop systems

- better support for clang
- support for module signing
- block device LED trigger
- includes recent cpupower
- amd-pstate EPP driver (beta)
- patches from Clear Linux
- patches from linux-hardened
- patches from Zen
- various fixes, optimisations and backports
- ProjectC schedulers
- sched/tip backports

See PKGBUILD for source and more details.
